package com.example.ficheros;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.IllegalFormatCodePointException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EscribirInternaActivity extends AppCompatActivity {

    public static final String NOMBRE_FICHERO = "resultado.txt";
    public static final String CODIFICACION = "UTF-8";
    Memoria memoria;

    @BindView(R.id.edTexto)
    EditText edTexto;

    @BindView(R.id.tvPropiedades)
    TextView tvPropiedades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escribir_interna);

        memoria = new Memoria(getApplicationContext());

        ButterKnife.bind(this);
    }

    @OnClick (R.id.btGuardar)
    public void guardar() {
        if (memoria.escribirInterna(NOMBRE_FICHERO, edTexto.getText().toString(), false, CODIFICACION)) {
            tvPropiedades.setText(memoria.mostrarPropiedadesInterna(NOMBRE_FICHERO));
        } else  {
            Toast.makeText(this, "No existe el fichero " + NOMBRE_FICHERO, Toast.LENGTH_SHORT).show();
        }
    }
}
